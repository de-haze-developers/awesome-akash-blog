# Awesome Akash Sites Links 

A curated link for blogs/websites/repos about Akash.network Decloud https://akash.network/


We will add new blogs/websites to this list wherever we can find info about akash.network. Pull request are welcomed with new links.



## List


- Introduction

  - [Akash A Decentralised cloud of future setup Guide](https://medium.com/dehazelabs/akashnet-a-decentralized-cloud-of-future-setup-guide-9e994e2dd866)

  - [Why Decentralized cloud is important for Defi](https://medium.com/stakin/why-decentralized-cloud-is-important-for-the-web-and-blockchain-industry-ffd0f1b239eb)

  - [Serum on Akash](https://github.com/ovrclk/serum-on-akash) : Official Repo sample

- Deployment
 
  - [How to use a custom domain for deployment with cloudflare](https://medium.com/nerd-for-tech/how-to-use-a-custom-domain-with-your-akash-deployment-5916585734a2)

  - [Guide to deploying Applications to DeCloud](https://medium.com/coinmonks/guide-to-deploying-applications-to-akash-decloud-b35dc97e5ca4)

  - [How to Deploy ThorChain BESwap on Akash DeCloud](https://medium.com/stakin/how-to-deploy-thorchain-bepswap-on-akash-decloud-%EF%B8%8F-6c4cfd158f38)

  - [Guide to Deploy Sifchain Node on Akash](https://medium.com/@minatofund/guide-to-deploy-sifchain-node-on-akash-67963246beb4)

  - [How to deploy Augur Client on Akash](https://wilsonlouie.medium.com/how-to-deploy-augur-client-on-akash-network-ab923f874644)

  - [Deploy a Wordpress Blog on Akash Network](https://medium.com/@zJ_/deploy-a-wordpress-blog-on-akash-network-d3f49a780e47)

  - [How to deploy a decentralized Blog on Akash](https://medium.com/@zJ_/how-to-deploy-a-decentralized-blog-3a5a13a6a827)

  - [Run Solana Cluster on Akash DeCloud](https://medium.com/@harishmarri551/run-solana-cluster-on-akash-decloud-8d04eb624a00)

- Provider

  - No Links yet

- Tools

  - [Kubespray](https://github.com/kubernetes-sigs/kubespray) : Config to setup Kubernetes cluster for Akash DeCloud

  - [Akashlytics](https://akashlytics.com/) : Tool providing analytics of Akash cloud network
  
  - [Deploy Tool](https://github.com/tombeynon/akash-deploy) : Open Source UI tool for easy deployments

  - [Skynet](https://siasky.net/) : Decentralized storage for using with Akash deployments

  - [SDL](https://github.com/ovrclk/docs/blob/b65f668b212ad1976fb976ad84a9104a9af29770/sdl/README.md) : SDL language syntax to write deployment scripts

- Sample Code Repo

  - [Awesome AKash](https://github.com/ovrclk/awesome-akash) : A curated list of configurations deployed on akash

- QA Forums

  - [Akash Forums](https://forum.akash.network/)


- Community
  
  - Global
    
    - [Discord](https://discord.com/invite/DxftX67)
    
    - [Twitter](https://twitter.com/akashnet_)
    
    - [Reddit](https://www.reddit.com/r/akashnetwork)

    - [Telegram](https://t.me/AkashNW)

    - [Instagram](https://instagram.com/akash.network)
  
  - China
    
    - [Weibo](https://weibo.com/akashchina)

    - [Bihu](https://bihu.com/people/1117023356)

    - [QQ](https://jq.qq.com/?_wv=1027&k=eB6YwtyH)

  - South Korea
    
    - [Telegram](https://t.me/AkashNW_KR)

    - [Kakao](https://open.kakao.com/o/gYSfKKKc)

  - Russia

    - [Telegram](https://t.me/akash_ru)


## About us

We [Dehazelabs](https://dehazelabs.com/) just love open source and use a lot in our projects. Akash DeCloud is a project which caught our attention, so we are contributing our efforts to the project with this list. Help us grow this list and share to people wanting to get hands on Akash.
